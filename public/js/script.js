/**
 * Created by Dima on 17.04.2016.
 */

$(document).ready(function () {
    console.log('Document Loaded ' + window.location);

    var box = $('#errorbox');

    var errors = [];

    if  ( $('#login_form').length ) {

        errors = [];

        $('#login_form').on('submit', function (e) {
            if ( !($('#login_form input[name="password"]').val()) ) {
                errors.push("Password value cannot be empty");

                e.preventDefault();
            }
        });
    }

    if  ( $('#register_form').length ) {

        $('#register_form').on('submit', function (e) {

            errors = [];
            inputs = [].slice.call($('#register_form input'));

            for (var i in inputs) {
                input = inputs[i];

                if (!$(input).val()) {
                    errors.push(
                        $(input).attr('placeholder') + ' cannot be empty'
                    );

                    e.preventDefault();
                }

            }

            if ( $('#register_form input[name="phone"]').val() && !validPhoneNumber($('#register_form input[name="phone"]').val()) ) {
                errors.push(
                    'Phone number is invalid'
                );

                e.preventDefault();
            }
        });

    }

    setInterval(function () {
        errormsg = '';

        for (var i in errors) {
            error = errors[i];

            if (i > 0) {
                errormsg += '<br>';
            }

            errormsg += error;
        }

        if (errors.length) {
            $(box).html(errormsg);
            $(box).show();
        }
    },1000);

});

function validPhoneNumber(phoneNumber) {
    return !!parseInt(phoneNumber);
}