<?php

class Application_Model_Orders extends Zend_Db_Table
{
    private static $statuses = [
        '-1' => 'Failed',
        '0' => 'Waiting',
        '1' => 'Success'
    ];
    
    protected $_name = 'Orders';

    public static function getStatusFromCode($code)
    {
        if (array_key_exists($code, self::$statuses)) {
            return self::$statuses[$code];
        }
        
        return 'Unknown status';
    }

}