<?php


class Application_Model_Users extends Zend_Db_Table
{
    protected $_name = 'Users';

    public static function getPasswordHash($password)
    {
        return md5(sha1(md5($password)));
    }

    public function get($id, $array = true)
    {
        $user = $this->fetchRow(
            $this
                ->select()
                ->where('ID = ?', $id)
        );

        if (!$user) {
            return [];
        }

        return $array ? $user->toArray() : $user;
    }
}