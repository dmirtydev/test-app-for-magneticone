<?php

class Application_Model_Products extends Zend_Db_Table
{
    protected $_name = 'Products';

    public function get($id, $array = true)
    {
        $product = $this->fetchRow(
            $this
                ->select()
                ->where('ID = ?', $id)
        );

        if (!$product) {
            return [];
        }

        return $array ? $product->toArray() : $product;
    }

    public function getProducts($order = false, $orderOrder = false)
    {
        if (!$order) {
            $order = 'added';
        }

        if (!$orderOrder) {
            $orderOrder = 'DESC';
        }

        return $this->fetchAll(
            $this
                ->select()
                ->order("$order $orderOrder")
        )->toArray();
    }
}