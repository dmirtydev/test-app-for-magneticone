<?php

require_once 'ApplicationController.php';

class UserController extends ApplicationController
{
    public function loginAction()
    {
        if(Zend_Auth::getInstance()->hasIdentity()){
            $this->_redirect('/');
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            $adapter = $this->getAdapter();
            $email = $request->getParam('email', false);
            $password = $request->getParam('password', false);

            $db = new Application_Model_Users();
            $query = $db->select()->where('email = ?', $email);
            if (!$db->fetchRow($query)) {
                echo "<script>alert('User with such email doesn\'t exists.');</script>";
                return;
            }

            $password = $db::getPasswordHash($password);

            $adapter->setIdentity($email)
                ->setCredential($password);
            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($adapter);
            if ($result->isValid()) {
                $identity = $adapter->getResultRowObject();
                $storage = $auth->getStorage();
                $storage->write($identity);
                $this->redirect('/');
            } else {
                echo "<script>alert(\"Username or password is wrong.\");</script>";
                $this->view->errorMessage = "Username or password is wrong";
                return;
            }
        }
    }


    public function registerAction()
    {
        $users = new Application_Model_Users();
        if($this->getRequest()->isPost()){

            $data = $this->getRequestParams();

            foreach($data as $item){
                if(empty($item)){
                    echo "<script>alert(\"Please enter all fields.\");</script>";
                    return;
                }
            }
            if (strlen($data['password']) <= 6){
                echo "<script>alert(\"Your password should be at least 6 characters.\");</script>";
                return;
            }
            if (strlen($data['phone']) !== 13){
                echo "<script>alert(\"Please fill the field Phone number in the format +38.\");</script>";
                return;
            }
            if ($data['password'] !== $data['confirm']) {
                echo "<script>alert(\"Password and confirm password don't match.\");</script>";
                return;
            }
            if(!(int)$data['phone']){
                echo "<script>alert(\"Phone number is not detected.\");</script>";
                return;
            }
            if ($data['password'] != $data['confirm']) {
                $this->view->errorMessage = "Password and confirm password don't match.";
                return;
            }

            $data['password'] = Application_Model_Users::getPasswordHash($data['password']);

            unset($data['confirm']);


            $data['ip'] = $_SERVER['REMOTE_ADDR'];

            $users->insert($data);
            $this->redirect('user/login');
        }
    }

    public function logoutAction()
    {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            Zend_Auth::getInstance()->clearIdentity();
        }

        $this->_redirect('/');
    }

    public function balanceAction()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/');
        }

        if ($this->getRequest()->isPost())
        {
            $balance = $this->getRequest()->getParam('balance', false);

            $userDb = new Application_Model_Users();
            $user = $userDb
                ->fetchRow(
                    $userDb
                        ->select()
                        ->where('email = ?', Zend_Auth::getInstance()->getIdentity()->email)
                );

            $userAssoc = $user->toArray();
            $userAssoc['balance'] += $balance;

            $user->setFromArray(
                $userAssoc
            );

            $user->save();
        }
    }

    private function getAdapter()
    {
        $adapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $adapter->setTableName('Users')
            ->setIdentityColumn('email')
            ->setCredentialColumn('password');
        return $adapter;
    }
}


