<?php

class ApplicationController extends Zend_Controller_Action
{
    /**
     * Initialize object
     *
     * Called from {@link __construct()} as final step of object instantiation.
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->view->assign('currentUser', $this->getCurrentUser());
    }


    protected function getRequestParams()
    {
        $params = $this->getRequest()->getParams();
        unset($params['controller']);
        unset($params['action']);
        unset($params['module']);

        return $params;
    }

    protected function getCurrentUser()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            return [];
        }

        $userDb = new Application_Model_Users();
        $user = $userDb
            ->fetchRow(
                $userDb
                    ->select()
                    ->where('email = ?', Zend_Auth::getInstance()->getIdentity()->email)
            )->toArray();

        return $user;
    }

    protected function getWaitingOrders($forCurrentUser = true)
    {
       return $this->getOrders(0, $forCurrentUser);
    }

    protected function getSuccessOrders($forCurrentUser = true)
    {
        return $this->getOrders(1, $forCurrentUser);
    }

    protected function getFailedOrders($forCurrentUser = true)
    {
        return $this->getOrders(-1, $forCurrentUser);
    }

    private function getOrders($status, $forCurrentUser = true)
    {
        $user = $this->getCurrentUser();
        $orderDb = new Application_Model_Orders();
        $productsDb = new Application_Model_Products();

        if ($forCurrentUser) {
            $orders = $orderDb->fetchAll(
                $orderDb
                    ->select()
                    ->where('user = ?', $user['ID'])
                    ->where('status = ?', $status)
                    ->order('created ASC')
            );    
        } else {
            $orders = $orderDb->fetchAll(
                $orderDb
                    ->select()
                    ->where('status = ?', $status)
                    ->order('created ASC')
            );
        }

        $ordersAssoc = $orders->toArray();

        foreach ($orders as $order) {
            $product = $productsDb->get($order->toArray()['product']);
            if ($product == []) {

                $orderAssoc = $order->toArray();
                $ordersAssoc['status'] = -1;

                $order->setFromArray($orderAssoc);
                $order->save();
            }
        }

        $ordersAssoc = array_map(
            function($order) use ($productsDb, $orderDb) {
                return array_merge(
                    ['productName' => $productsDb->get($order['product'])['name']],
                    $order
                );
            },
            $ordersAssoc
        );

        return $ordersAssoc;
    }
}