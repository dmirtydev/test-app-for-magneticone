<?php

require_once 'ApplicationController.php';

class AdminController extends ApplicationController
{
    public function init()
    {
        if ($this->getCurrentUser()['role'] < 3) {
//            echo 'Forbidden';
            throw new Exception();
        }
    }

    public function indexAction()
    {
        
    }

    public function deleteProductAction()
    {
        $id = $this->getRequest()->getParam('product', false);

        if ($id) {
            $productDb = new Application_Model_Products();
            $productDb->delete("ID = $id");
        }

        $this->_redirect('/admin/product-list');
    }

    public function categoryListAction()
    {
        $categoryDb = new Application_Model_Categories();

        $categories = $categoryDb->fetchAll(
            $categoryDb->select()
        )->toArray();

        $this->view->assign('categories', $categories);
    }

    public function productListAction()
    {
        $productDb = new Application_Model_Products();
        $products = $productDb->fetchAll(
            $productDb->select()
        )->toArray();

        $userDb = new Application_Model_Users();

        $products = array_map(function($product) use ($userDb) {
            return array_merge(
                $product,
                ['createdBy' => $userDb->get($product['user'])['email']]
            );
        }, $products);

        $this->view->assign('products', $products);
    }

    public function usersListAction()
    {
        $usersDb = new Application_Model_Users();
        $users = $usersDb->fetchAll(
            $usersDb->select()
        )->toArray();

        $this->view->assign('users', $users);
    }

    public function newProductAction()
    {
        $productDb = new Application_Model_Products();
        $product = $productDb->get($this->getRequest()->getParam('product', false), false);
        if ($product) {
            $productAssoc = $product->toArray();
            $this->view->assign('product', $productAssoc);
        }

        $categoryDb = new Application_Model_Categories();
        $categories = $categoryDb->fetchAll(
            $categoryDb
                ->select()
        )->toArray();



        $this->view->assign('categories', $categories);
        $this->view->assign('currentUser', $this->getCurrentUser());

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequestParams();

            $adapter = new Zend_File_Transfer_Adapter_Http();

            $adapter->setDestination(APPLICATION_PATH . '/../public/data/files/products');
            $filename = $adapter->getFileName('image');
            $filenameWithoutPath = $adapter->getFileName('image', false);

            if (!$adapter->receive()) {
                $messages = $adapter->getMessages();
                echo implode("<br>", $messages) . '<br>';
            }

            if (empty($filename) || !file_exists($filename)) {
                echo "<br>File wasn't uploaded";
                return;
            }

            $data['image'] = '/data/files/products/' . $filenameWithoutPath;

            if (!$data['ID']) {
                $id = $productDb->insert($data);
            } else {
                $product = $productDb->get($data['ID'], false);
                $product->setFromArray($data);
                $product->save();
                $id = $data['ID'];
            }

            $this->redirect('/shop/details/product/' .$id);
        }
    }

    public function newCategoryAction()
    {
        $categoryDb = new Application_Model_Categories();
        $categories = $categoryDb->fetchAll(
            $categoryDb
                ->select()
        )->toArray();

        $this->view->assign('categories', $categories);

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequestParams();

            $id = $categoryDb->insert($data);

            $this->redirect('/shop/categories/category/' . $id);
        }
    }

    public function ordersAction()
    {
        $type = $this->getRequest()->getParam('type', false);
        
        switch ($type) {
            case 'success':
                $orders = $this->getSuccessOrders(false);
                break;
            case 'failed':
                $orders = $this->getFailedOrders(false);
                break;
            case 'waiting':
                $orders = $this->getWaitingOrders(false);
                break;
            default:
                $orders = array_merge(
                    $this->getSuccessOrders(false),
                    $this->getFailedOrders(false),
                    $this->getWaitingOrders(false)
                );
        }
        
        $userDb = new Application_Model_Users();
        $productDb = new Application_Model_Products();

        $orders = array_map(
            function($order) use ($userDb, $productDb) {
                $user = @$userDb->get($order['user']);
                $order['userEmail'] = !empty($user) ? $user['email'] : 'User doesn\'t exists';

                $product = $productDb->get($order['product']);
                $order['productName'] = !empty($product) ? $product['name'] : 'Product doesn\'t exists';

                $order['status'] = Application_Model_Orders::getStatusFromCode($order['status']);
                
                return $order;
            },
            $orders
        );
        
        $this->view->assign('orders', $orders);
    }
}