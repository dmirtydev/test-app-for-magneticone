<?php
/**
 * IndexController.php
 * 
 * @author Kathryn Reeve <k.reeve@ctidigital.com>
 */
/**
 * IndexController
 *   - controller to handle registration of customers
 *   
 */
class IndexController extends Zend_Controller_Action
{
    public function indexAction(){
        $order = $this->getRequest()->getParam('order', false);
        $productsDb = new Application_Model_Products();

        if ($order && in_array($order, ['name', 'name_DESC', 'price', 'price_DESC'])){
            $orders = explode('_', $order);
            
            $order = $orders[0];
            $orderOrder = @$orders[1] ? $orders[1] : 'ASC';
            
            $products = $products = $productsDb->getProducts($order, $orderOrder);
        } else {
            $products = $productsDb->getProducts();
        }


        $this->view->assign('products', $products);
    }

    public function infoAction()
    {

    }
}