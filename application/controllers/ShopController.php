<?php

require_once 'ApplicationController.php';

class ShopController extends ApplicationController
{
    public function detailsAction()
    {
        $user = $this->getCurrentUser();
        $this->view->assign('user', $user['ID']);

        $id = $this->getRequest()->getParam('product', false);
        $productsDb = new Application_Model_Products();
        $product = $productsDb->get($id);
        $this->view->assign('product', $product);
    }

    public function categoriesAction()
    {
        $category = $this->getRequest()->getParam('category', false);

        if ($category) {
            $categoryDb = new Application_Model_Categories();
            $category = $categoryDb->fetchRow(
                $categoryDb
                    ->select()
                    ->where('ID = ?', $category)
            );

            $this->view->assign('categoryName', $category['name']);

            $productDb = new Application_Model_Products();
            $products =
                $productDb->fetchAll(
                    $productDb
                        ->select()
                        ->where('category = ?', $category['ID'])
                        ->order('added DESC')
                )->toArray();

            $this->view->assign('products', $products);

        }
    }

    public function addToCartAction()
    {
        $user = $this->getCurrentUser();
        $data = $this->getRequestParams();
        if (empty($user)){
            $this->redirect('/user/login');
        }
        $orderDb = new Application_Model_Orders();
        $data['user'] = $user['ID'];
        $orderDb->insert($data);

        $this->redirect('/shop/cart');
    }

    public function cartAction()
    {
        $orders = $this->getWaitingOrders();
        $this->view->assign('orders', $orders);
    }

    public function confirmPaymentAction()
    {
        $this->view->assign('generalError', false);
        $user = $this->getCurrentUser();
        $orders = $this->getWaitingOrders();
        $productDb = new Application_Model_Products();
        $requiredAmount = 0;

        foreach ($orders as $key => $order) {
            $product = $productDb->get($order['product']);
            if ($product['count'] > 0) {
                $requiredAmount += $product['price'];
                $orders[$key]['price'] = $product['price'];
            } else {
                $orders[$key]['notAvailable'] = true;
            }
        }

        if ($user['balance'] < $requiredAmount) {
            $this->view->assign('generalError',
            "Not enough money. Got to <a href='/user/balance'>balance page </a> and add $" . ($requiredAmount - $user['balance']) . ' more');
        }

        $this->view->assign('orders', $orders);
        $this->view->assign('total', $requiredAmount);
    }

    public function historyAction()
    {
        $this->view->assign('orders', $this->getSuccessOrders());
    }

    public function confirmAction()
    {
        $user = $this->getCurrentUser();
        $amount = $this->getRequest()->getParam('amount', false);

        if ($user['balance'] < $amount) {
            $this->_redirect('/shop/cart');
            return;
        }

        $orders = $this->getWaitingOrders();
        $productDb = new Application_Model_Products();
        $ordersDb = new Application_Model_Orders();
        $usersDb = new Application_Model_Users();

        foreach ($orders as $order) {
            $product = $productDb->get($order['product']);
            $available = ($product['count'] > 0);

            if ($available) {
                $order['status'] = 1;
                unset($order['productName']);
                $ordersDb->update($order, $ordersDb->getAdapter()->quoteInto('ID = ?', $order['ID']));
                $productDb->update(['count' => $product['count'] - 1], $productDb->getAdapter()->quoteInto('ID = ?', $product['ID']));
            }
        }

        $usersDb->update(['balance' => $user['balance'] - $amount], $usersDb->getAdapter()->quoteInto('ID = ?', $user['ID']));

        $this->redirect('/shop/history');
    }

    public function cancelOrderAction()
    {
        $user = $this->getCurrentUser();
        $order = $this->getRequest()->getParam('order', false);

        $orderDb = new Application_Model_Orders();
        $order = $orderDb->fetchRow(
            $orderDb
                ->select()
                ->where('ID = ?', $order)
        );

        $orderAssoc = $order->toArray();

        if ($orderAssoc && $orderAssoc['user'] == $user['ID']) {
            $orderAssoc['status'] = -1;
            $order->setFromArray($orderAssoc);
            $order->save();
        }

        $this->redirect('/shop/cart');
    }
}
