<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initAutoload()
    {
        $autoloader = new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Default_',
            'basePath' => dirname(__FILE__),
        ));

        return $autoloader;
    }

    public function _initRoutes()
    {
        $router = Zend_Controller_Front::getInstance()->getRouter();


        $routeInfo = new Zend_Controller_Router_Route(
            'info',
            array(
                'module' => 'default',
                'controller' => 'index',
                'action' => 'info'
            )
        );

        $routeUser = new Zend_Controller_Router_Route(
            'user[/:action]',
            [
                'module' => 'default',
                'controller' => 'user'
            ]
        );

        $router->addRoute('info', $routeInfo);
        $router->addRoute('user', $routeUser);
    }
}