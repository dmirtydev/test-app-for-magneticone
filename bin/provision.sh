#!/usr/bin/env bash

#Update Centos
sudo yum -y update

#Install tools
sudo yum install -y htop tar bzip2 fontconfig git strace sendmail vixie-cron gcc openssl-devel libmemcached-devel cyrus-sasl-devel

#Install LAMP
sudo yum install -y httpd
sudo echo "127.0.0.1 test-app.dev" >> /etc/hosts
sudo /etc/init.d/httpd start

sudo yum remove -y mysql-libs-5.1.73-5.el6_6.x86_64
sudo yum install -y mysql55w mysql55w-server
sudo /etc/init.d/mysqld start

sudo yum install -y php54w.x86_64
sudo yum install -y php54w-pear php54w-devel.x86_64 php54w-pdo.x86_64 php54w-memcached.x86_64 php54w-bcmath.x86_64 php54w-dom.x86_64 php54w-eaccelerator.x86_64 php54w-gd.x86_64 php54w-imap.x86_64 php54w-intl.x86_64 php54w-mbstring.x86_64 php54w-mcrypt.x86_64 php54w-mysql.x86_64 php54w-posix.x86_64 php54w-soap.x86_64 php54w-tidy.x86_64 php54w-xmlrpc.x86_64

sudo yum install -y phpunit
sudo pear channel-discover pear.pdepend.org
sudo pear channel-discover pear.phpmd.org
sudo pear channel-discover pear.mongo.org
sudo pear install PHP_CodeSniffer
sudo pear install phpmd/PHP_PMD

sudo pecl install mongo

#Add mongo extension
sudo echo 'extension=mongo.so' >> /etc/php.ini

sudo chmod 777 -R /var/lib/php/session

sudo /etc/init.d/httpd restart